package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Group struct {
	ID       int
	Number   string
	Year     int
	Students []Student
}

type Student struct {
	LastName   string
	FirstName  string
	MiddleName string
	Birthday   string
	Address    string
	Phone      string
	Rating     []int
}

func (war *Group) StudentsNumber() int {
	return len(war.Students)
}

func (war *Group) AverageScore() float64 {
	var scoreSum int
	for _, student := range war.Students {
		for _, score := range student.Rating {
			scoreSum += score
		}
	}
	return float64(scoreSum) / float64(war.StudentsNumber())
}

type AverageScore struct {
	Average float64
}

func main() {
	data, _ := ioutil.ReadAll(os.Stdin)
	s := new(Group)
	_ = json.Unmarshal(data, &s)
	ratio := new(AverageScore)
	ratio.Average = s.AverageScore()
	dataToPrint, _ := json.Marshal(ratio)
	fmt.Printf("%s", dataToPrint)
}
