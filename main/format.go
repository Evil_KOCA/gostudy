package main

import "fmt"

func main() {
	var a string
	fmt.Printf("\"!\"\n")         // "!"
	fmt.Printf("\\!\\\"\n")       // "!"
	fmt.Printf("\b123\f456\f789") // "!"
	fmt.Printf("\vf\v")
	fmt.Printf("\r Input ")
	fmt.Scan(&a)
	fmt.Printf("123\b")
	fmt.Scan(&a)
}
