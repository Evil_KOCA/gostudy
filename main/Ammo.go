package main

import "fmt"

type T1000 struct {
	On    bool
	Ammo  int
	Power int
}

func (war *T1000) Shoot() bool {
	if war.On && war.Ammo > 0 {
		war.Ammo--
		return true
	}
	return false
}
func (war *T1000) RideBike() bool {
	if war.On && war.Power > 0 {
		war.Power--
		return true
	}
	return false
}

func main() {
	testStruct := new(T1000)
	fmt.Println(testStruct)
}
