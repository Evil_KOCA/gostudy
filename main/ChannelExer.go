package main

import (
	"fmt"
)

func main() {
	<-myFunc()
}

func myFunc() <-chan struct{} {
	done := make(chan struct{})
	go func() {
		fmt.Println("hello")
		close(done)
	}()
	return done
}

//func main() {
//	done := make(chan struct{})
//	go myFunc(done)
//	<-done
//}
//
//func myFunc(done chan struct{}) {
//	fmt.Println("hello")
//	close(done)
//}

//func main() {
//	c := make(chan int, 5)
//	for i := 0; i < 5; i++ {
//		go myFunc(c)
//	}
//	fmt.Print(<-c)
//}
//
//func myFunc(c chan int) {
//	c <- cap(c)
//}
//
//func removeDuplicates(inputStream, outputStream chan string) {
//	var curString string
//	for auf := range inputStream {
//		if auf != curString {
//			outputStream <- auf
//			curString = auf
//		}
//	}
//	close(outputStream)
//}

//func main() {
//	c := make(chan int, 1) // здесь 1 - размер буфера
//	fmt.Println(len(c), cap(c)) // 0 1
//	c <- 1
//	fmt.Println(len(c), cap(c)) // 1 1
//	<-c
//	pipe := make(chan int)     // небуферизованный канал
//	pipe := make(chan int, 0)  // небуферизованный канал
//	pipe := make(chan int, 5)  // буферизованный канал с ёмкостью 5
//}
