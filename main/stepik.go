//package main
//
//import (
//	"fmt"
//)
//
//func Max(values []int) (min int) {
//
//	min = values[0]
//	for _, v := range values {
//		if v > min {
//			min = v
//		}
//	}
//
//	return min
//}
//
//func main() {
//	var d, count, result int
//	var dest []int
//	fmt.Scanln(&d)
//	for d != 0 {
//		dest = append(dest, d)
//		fmt.Scan(&d)
//		if d%8 == 0 && 100 > d && d > 9 {
//			result += d
//		}
//	}
//	for _, i := range dest {
//		if i == Max(dest){
//			count += 1
//		}
//	}
//	fmt.Println(count)
//}

package main

import (
	"fmt"
)

func percentage(x, p, y int) int {
	var yer int
	for x < y {
		x = x * (p + 100) / 100
		yer += 1
	}
	return yer
}
func main() {
	var syap float64
	fmt.Scanln(&syap)
	if syap <= 0 {
		fmt.Printf("число %2.2f не подходит\n", syap)
	} else if syap > 10000 {
		fmt.Printf("%e\n", syap)
	} else {
		fmt.Printf("%.4f\n", syap*syap)
	}
}
