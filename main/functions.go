package main

import "fmt"

func minimumFromFour() int {
	var a, b, c, d int
	fmt.Scan(&a)
	fmt.Scan(&b)
	fmt.Scan(&c)
	fmt.Scan(&d)
	arr := [4]int{a, b, c, d}
	var result int
	result = arr[0]
	for i := 1; i < 4; i++ {
		if result < arr[i] {
			result = arr[i]
		}
	}
	return result
}

func vote() int {
	var x, y, z int
	fmt.Scan(&x)
	fmt.Scan(&y)
	fmt.Scan(&z)
	if x+y+z < 2 {
		return 0
	} else {
		return 1
	}
}

func fibonacci(n int) int {
	switch n {
	case 1:
		return 1
	case 2:
		return 1
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func sumInt(urc ...int) (int, int) {
	var surf, result int
	for _, elem := range urc {
		surf++
		result += elem
	}
	return surf, result
}

func main() {
	fmt.Println(fibonacci(4))
	a, b := sumInt(1, 0, 6, 6, 40)
	fmt.Println(a, b)
}
