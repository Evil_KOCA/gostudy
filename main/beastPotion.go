package main

import "fmt"

type BeastPotion struct {
	messengerPotion  MessengerPotion  // 3
	experiencePotion ExperiencePotion // 3
	huntPotion       HuntPotion       // 3
	wainingMoonTears string           // 1
}

func NewBeastPotion() BeastPotion {
	NewMessengerPotion := BeastPotion{
		messengerPotion:  NewMessengerPotion(),
		experiencePotion: NewExperiencePotion(),
		huntPotion:       NewHuntPotion(),
		wainingMoonTears: "Слезы Убывающей Луны",
	}
	return NewMessengerPotion
}

func (beastPotion *BeastPotion) CountSimpleReagents() map[string]int {
	messengerPotionRoster := beastPotion.messengerPotion.CountSimpleReagents()
	experiencePotionRoster := beastPotion.experiencePotion.CountSimpleReagents()
	huntPotionRoster := beastPotion.huntPotion.CountSimpleReagents()
	roster := map[string]int{
		beastPotion.wainingMoonTears: 2,
	}
	for key, _ := range messengerPotionRoster {
		roster[key] += messengerPotionRoster[key] * 3
	}
	for key, _ := range experiencePotionRoster {
		roster[key] += experiencePotionRoster[key] * 3
	}
	for key, _ := range huntPotionRoster {
		roster[key] += huntPotionRoster[key] * 3
	}
	return roster
}

type MessengerPotion struct {
	powderReagent  PowderReagent // 4
	durabilityOil  DurabilityOil // 1
	wavelet        string        // 2
	monkBranch     string        // 2
	powderOfOrigin string        // 4
}

func NewMessengerPotion() MessengerPotion {
	NewMessengerPotion := MessengerPotion{
		powderReagent:  NewPowderReagent(),
		durabilityOil:  NewDurabilityOil(),
		wavelet:        "Волнушка",
		monkBranch:     "Ветвь Монаха",
		powderOfOrigin: "Порошок Изначальности",
	}
	return NewMessengerPotion
}

func (messengerPotion *MessengerPotion) CountSimpleReagents() map[string]int {
	powderReagentRoster := messengerPotion.powderReagent.CountSimpleReagents()
	durabilityOilRoster := messengerPotion.durabilityOil.CountSimpleReagents()
	roster := map[string]int{
		messengerPotion.wavelet:        2,
		messengerPotion.monkBranch:     2,
		messengerPotion.powderOfOrigin: 4,
	}
	for key, _ := range powderReagentRoster {
		roster[key] += powderReagentRoster[key] * 4
	}
	for key, _ := range durabilityOilRoster {
		roster[key] += durabilityOilRoster[key] * 1
	}
	return roster
}

type ExperiencePotion struct {
	jesterBlood JesterBlood // 1
	fireDust    string      // 2
	pineJuice   string      // 5
	clover      string      // 7
}

func NewExperiencePotion() ExperiencePotion {
	NewHuntPotion := ExperiencePotion{
		jesterBlood: NewJesterBlood(),
		fireDust:    "Пыль Тьмы",
		pineJuice:   "Сок Сосны",
		clover:      "Клевер",
	}
	return NewHuntPotion
}

func (experiencePotion *ExperiencePotion) CountSimpleReagents() map[string]int {
	jesterBloodRoster := experiencePotion.jesterBlood.CountSimpleReagents()
	roster := map[string]int{
		experiencePotion.fireDust:  2,
		experiencePotion.pineJuice: 5,
		experiencePotion.clover:    7,
	}
	for key, _ := range jesterBloodRoster {
		roster[key] += jesterBloodRoster[key] * 1
	}
	return roster
}

type HuntPotion struct {
	powderReagent   PowderReagent // 1
	distillWater    string        // 3
	bloodGroupThree string        // 6
	calendula       string        // 4
}

func NewHuntPotion() HuntPotion {
	NewHuntPotion := HuntPotion{
		powderReagent:   NewPowderReagent(),
		distillWater:    "Очищенная Вода",
		bloodGroupThree: "Кровь III Группы",
		calendula:       "Календула",
	}
	return NewHuntPotion
}

func (huntPotion *HuntPotion) CountSimpleReagents() map[string]int {
	powderReagentRoster := huntPotion.powderReagent.CountSimpleReagents()
	roster := map[string]int{
		huntPotion.distillWater:    3,
		huntPotion.bloodGroupThree: 6,
		huntPotion.calendula:       4,
	}
	for key, _ := range powderReagentRoster {
		roster[key] += powderReagentRoster[key] * 1
	}
	return roster
}

type DurabilityOil struct {
	jesterBlood JesterBlood // 1
	monkBranch  string      // 1
	natureFruit string      // 1
	fireDust    string      // 1
}

func NewDurabilityOil() DurabilityOil {
	NewDurabilityOil := DurabilityOil{
		jesterBlood: NewJesterBlood(),
		monkBranch:  "Ветвь Монаха",
		natureFruit: "Плод Природы",
		fireDust:    "Пыль Огня",
	}
	return NewDurabilityOil
}

func (durabilityOil *DurabilityOil) CountSimpleReagents() map[string]int {
	jesterBloodRoster := durabilityOil.jesterBlood.CountSimpleReagents()
	roster := map[string]int{
		durabilityOil.monkBranch:  1,
		durabilityOil.natureFruit: 1,
		durabilityOil.fireDust:    1,
	}
	for key, _ := range jesterBloodRoster {
		roster[key] += jesterBloodRoster[key] * 1
	}
	return roster
}

type JesterBlood struct {
	liquidReagent   LiquidReagent // 1
	dustOfDarkness  string        // 1
	fairyLeaf       string        // 1
	bloodGroupThree string        // 2
}

func NewJesterBlood() JesterBlood {
	NewJesterBlood := JesterBlood{
		liquidReagent:   NewLiquidReagent(),
		dustOfDarkness:  "Пыль Тьмы",
		fairyLeaf:       "Волшебный Плод",
		bloodGroupThree: "Кровь III Группы",
	}
	return NewJesterBlood
}

func (jesterBlood *JesterBlood) CountSimpleReagents() map[string]int {
	liquidReagentRoster := jesterBlood.liquidReagent.CountSimpleReagents()
	roster := map[string]int{
		jesterBlood.dustOfDarkness:  1,
		jesterBlood.fairyLeaf:       1,
		jesterBlood.bloodGroupThree: 2,
	}
	for key, _ := range liquidReagentRoster {
		roster[key] += liquidReagentRoster[key] * 1
	}
	return roster
}

type LiquidReagent struct {
	distillWater string // 1
	weed         string // 1
	calendula    string // 1
	salt         string // 1
}

func NewLiquidReagent() LiquidReagent {
	NewLiquidReagent := LiquidReagent{
		distillWater: "Очищенная Вода",
		weed:         "Сорняк",
		calendula:    "Календула",
		salt:         "Соль",
	}
	return NewLiquidReagent
}

func (liquidReagent *LiquidReagent) CountSimpleReagents() map[string]int {
	roster := map[string]int{
		liquidReagent.distillWater: 1,
		liquidReagent.weed:         1,
		liquidReagent.calendula:    1,
		liquidReagent.salt:         1,
	}
	return roster
}

type PowderReagent struct {
	distillWater string // 1
	weed         string // 1
	azalea       string // 1
	sugar        string // 1
}

func NewPowderReagent() PowderReagent {
	NewPowderReagent := PowderReagent{
		distillWater: "Очищенная Вода",
		weed:         "Сорняк",
		azalea:       "Азалея",
		sugar:        "Сахар",
	}
	return NewPowderReagent
}

func (powderReagent *PowderReagent) CountSimpleReagents() map[string]int {
	roster := map[string]int{
		powderReagent.distillWater: 1,
		powderReagent.weed:         1,
		powderReagent.azalea:       1,
		powderReagent.sugar:        1,
	}
	return roster
}

type Ingredient interface {
	CountSimpleReagents() map[string]int
}

func PrettyPrint(roster map[string]int) {
	fmt.Println("Полный список ингридиетов для одной попытки крафта зелья Зверя")
	for key, value := range roster {
		fmt.Printf("%s в количестве %d", key, value)
		fmt.Println()
	}
}

func main() {
	var potion = NewBeastPotion()
	PrettyPrint(potion.CountSimpleReagents())
}
