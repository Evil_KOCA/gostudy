//package main
//
//import (
//	"fmt"
//	"time"
//)
//
//func out(from, to int, ch chan bool) {
//	for i := from; i <= to; i++ {
//		time.Sleep(50 * time.Millisecond)
//		fmt.Println(i)
//	}
//	ch <- true
//}
//
//func main() {
//	ch := make(chan bool)
//
//	go out(0, 5, ch)
//	go out(6, 10, ch)
//
//	<-ch
//	<-ch
//}

package main

import (
	"fmt"
)

func main() {
	done := make(chan struct{})
	go myFunc1(done)
	<-done
}

func myFunc1(done chan struct{}) {
	fmt.Println("hello")
	close(done)
}
