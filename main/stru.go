//package main
//
//import (
//	"fmt"
//	"math"
//)
//
//type Circle struct {
//	x float64
//	y float64
//	r float64
//}
//
//func circleArea(c *Circle) float64 {
//	return math.Pi * c.r * c.r
//}
//
//func (c *Circle) area() float64 {
//	return math.Pi * c.r * c.r
//}
//func distance(x1, y1, x2, y2 float64) float64 {
//	a := x2 - x1
//	b := y2 - y1
//	return math.Sqrt(a*a + b*b)
//}
//
//type Rectangle struct {
//	x1, y1, x2, y2 float64
//}
//
//func (r *Rectangle) area() float64 {
//	l := distance(r.x1, r.y1, r.x1, r.y2)
//	w := distance(r.x1, r.y1, r.x2, r.y1)
//	return l * w
//}
//
//func main() {
//	c := Circle{0, 0, 5}
//	fmt.Println(circleArea(&c))
//	fmt.Println(c.area())
//	r := Rectangle{0, 0, 10, 10}
//	fmt.Println(r.area())
//}

package main

import "fmt"

type Phone struct {
	Main       string
	Additional string
}
type Person struct {
	Name    string
	Surname string
	Age     int
	Phone   Phone
}

func main() {
	kat := Person{
		Name:    "Katrin",
		Surname: "Pirson",
		Age:     38,
		Phone: Phone{
			Main:       "89542642321",
			Additional: "1234",
		},
	}
	fmt.Println(kat)
	kat.Phone.Main = "12"
	fmt.Println(kat)
}
