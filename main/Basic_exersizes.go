//package main
//
//import "fmt"
//
//func main() {
//	var a, b int
//	fmt.Scanln(&a)
//	b = a / 100 + ((a % 100) / 10) * 10 + ((a % 100) % 10) * 100
//	fmt.Print(b)
//}

//package main
//
//import "fmt"
//
//func main() {
//	var a,b,c int
//	fmt.Scan(&a)
//	fmt.Scan(&b)
//	fmt.Scan(&c)
//	if a + b > c && a + c > b && b + c > a {
//		fmt.Println("Существует")
//	} else {
//		fmt.Println("Не существует")
//	}
//}

//package main
//
//import "fmt"
//
//func main() {
//	var a, b, d int
//
//	fmt.Scanln(&a)
//	fmt.Scanln(&b)
//	for i := 0; i < 7; i++ {
//		if (b-i)%7 == 0 && b-i > a {
//			fmt.Println(b - i)
//			d = 1
//			break
//		}
//	}
//	if d == 0 {
//		fmt.Println("NO")
//	}
//}

//package main
//
//import "fmt"
//
//func main() {
//	var a,b int
//
//	fmt.Scanln(&a)
//	b = a % 10
//	switch {
//	case b == 5 || b == 6 || b == 7 || b == 8 || b == 9 || b == 0 || a > 10 && a < 20:
//		fmt.Println(a, "korov")
//	case b == 1:
//		fmt.Println(a, "korova")
//	case b == 2 || b == 3 || b == 4 :
//		fmt.Println(a, "korovy")
//	}
//}

//package main
//
//import "fmt"
//
//func main() {
//	var a,b int
//	b = 1
//
//	fmt.Scanln(&a)
//	if a == 1 {
//		fmt.Println(1)
//	} else {
//		for b <= a {
//			if b == 1 {
//				fmt.Print(b)
//				b = 2
//			} else {
//				fmt.Print(" ", b)
//				b *= 2
//			}
//		}
//	}
//}

package main

import "fmt"

func main() {
	var a, fibcur, fibprev, counter int
	fibcur = 2
	fibprev = 1
	counter = 4

	fmt.Scanln(&a)
	for i := 0; i < a; i++ {
		fibcur = fibprev + fibcur
		fibprev = fibcur - fibprev
		if a == fibcur {
			fmt.Println(counter)
			break
		}
		if a < fibcur && a > fibprev {
			fmt.Println(-1)
			break
		}
		counter++
	}
}
