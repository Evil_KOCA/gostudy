package main

import (
	"errors"
	"fmt"
)

func divide(a int, b int) (int, error) {
	return a / b, errors.New("ошибка")
}

func main() {
	var a, b int
	_, err := fmt.Scan(&a) // функция Scan возвращает два параметра, но нам сейчас важно проверить только ошибку
	if err != nil {
		fmt.Println("ошибка")
	}
	_, err1 := fmt.Scan(&b)
	if err1 != nil {
		fmt.Println("ошибка")
	}
	a, err2 := divide(a, b)
	if err2 != nil {
		fmt.Println("ошибка")
	} else {
		fmt.Println(a)
	}
}
