//https://tour.golang.org/flowcontrol/8

package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) (float64, int) {
	z := float64(2.)
	s := float64(0)
	var counter int
	for {
		z = z - (z*z-x)/(2*z)
		if math.Abs(s-z) < 1e-15 {
			break
		}
		s = z
		counter += 1
	}
	return s, counter
}

func main() {
	fmt.Println(Sqrt(40))
	fmt.Println(math.Sqrt(40))
}
