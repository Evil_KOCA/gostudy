package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	//s = "12 мин. 13 сек."
	const now = 1589570165

	rd := bufio.NewReader(os.Stdin)
	timeStr, err := rd.ReadString('\n')
	if err != nil {
		//...
	}
	timeStr = strings.TrimSuffix(timeStr, "\n")
	timeStr = strings.Replace(timeStr, " мин. ", "m", 1)
	timeStr = strings.Replace(timeStr, " сек.", "s", 1)

	nowTime := time.Unix(now, 0)
	nowTime.UTC()

	dur, _ := time.ParseDuration(timeStr)
	future := nowTime.Add(dur)
	fmt.Println(future.Format(time.UnixDate))
}

//func main()  {
//	//s = "13.03.2018 14:00:15,12.03.2018 14:00:15"
//	rd := bufio.NewReader(os.Stdin)
//	timeStr, err := rd.ReadString('\n')
//	if err != nil {
//		//...
//	}
//	timeStr = strings.TrimSuffix(timeStr, "\n")
//	times := strings.Split(timeStr, ",")
//	left, _ := time.Parse("02.01.2006 15:04:05", times[0])
//	right, _ := time.Parse("02.01.2006 15:04:05", times[1])
//	if right.Sub(left) > 0 {
//		fmt.Println(right.Sub(left))
//	} else {
//		fmt.Println(left.Sub(right))
//	}
//}

//func main()  {
//	//s = "2020-05-15 13:00:00"
//	rd := bufio.NewReader(os.Stdin)
//	timeStr, err := rd.ReadString('\n')
//	if err != nil {
//		//...
//	}
//
//	// Удаление последнего символа при помощи пакета "strings".
//	timeStr = strings.TrimSuffix(timeStr, "\n")
//	rect, _ := time.Parse("2006-01-02 15:04:05", timeStr)
//	if rect.Hour() < 13 {
//		fmt.Println(rect.Format("2006-01-02 15:04:05"))
//	} else {
//		tria := rect.Add(time.Hour * 24)
//		fmt.Println(tria.Format("2006-01-02 15:04:05"))
//	}
//
//}

//func main()  {
//	var s string
//	_, err1 := fmt.Scanln(&s)
//	if err1 != nil {
//		panic(err1)
//	}
//	strings.TrimSpace(s)
//	//s := "1986-04-16T05:20:00+06:00"
//	rect, err := time.Parse(time.RFC3339, s)
//	if err != nil {
//		panic(err)
//	}
//	fmt.Println(rect.Format(time.UnixDate))
//
//}
