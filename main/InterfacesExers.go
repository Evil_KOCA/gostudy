//package main
//
//import "fmt"
//
//type Inetka interface {}
//
//func readTask() (Inetka,Inetka,Inetka){
//	return true, "5.7", "+"
//}
//
//func main() {
//	value1, value2, operation := readTask()
//	if _, ok := value1.(float64); !ok {
//		fmt.Printf("value=%v: %T", value1, value1)
//		return
//	}
//	if _, ok := value2.(float64); !ok {
//		fmt.Printf("value=%v: %T", value2, value2)
//		return
//	}
//	if _, ok := operation.(string); !ok {
//		fmt.Println("неизвестная операция")
//		return
//	}
//	switch operation {
//	case "+":
//		fmt.Printf("%.4f", value1.(float64)+value2.(float64))
//	case "-":
//		fmt.Printf("%.4f", value1.(float64)-value2.(float64))
//	case "*":
//		fmt.Printf("%.4f", value1.(float64)*value2.(float64))
//	case "/":
//		fmt.Printf("%.4f", value1.(float64)/value2.(float64))
//	default:
//		fmt.Println("неизвестная операция")
//	}
//}
package main

import (
	"fmt"
	"strings"
)

type Battery struct {
	Charge string
}

func (war *Battery) String() string {
	chargeLeft := strings.Count(war.Charge, "1")
	result := fmt.Sprintf("[%s%s]", strings.Repeat(" ", 10-chargeLeft), strings.Repeat("X", chargeLeft))
	return result
}
func main() {
	batteryForTest := new(Battery)
	var input string
	fmt.Scan(&input)
	batteryForTest.Charge = input
}
