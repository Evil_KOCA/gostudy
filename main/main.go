// HKRjm-XaNepm5mk6gAbR
//package main
//
//import "fmt"

//func main() {

//var input string
//fmt.Scanln(&input)
//
//fmt.Println(input)
//if x := 42; x > 18 {
//	fmt.Println("Разрешено")
//} else {
//	fmt.Println("Не разрешено")
//}
//
//x := 14
//
//if x > 18 {
//	fmt.Println("Разрешено")
//} else {
//	fmt.Println("Не разрешено")
//
//var score = 80
//if score >= 75 {
//	fmt.Println("Проходи")
//}
//
//
//var a int
//var b int
//fmt.Scanln(&a)
//fmt.Scanln(&b)
//fmt.Println(a + b)
//
//
//num := 3
//
//if num == 1 {
//	fmt.Println("Один")
//} else if num == 2 {
//	fmt.Println("Два")
//} else  if num == 3 {
//	fmt.Println("Три")
//} else {
//	fmt.Println(num)
//}
//
//
//num := 3
//
//switch num {
//	case 1:
//		fmt.Println("One")
//	case 2:
//		fmt.Println("Two")
//	case 3:
//		fmt.Println("Three")
//	default:
//		fmt.Println(num)
//}

//var x int = 42
//var y float32 = 1.37
//var name string = "James"
//var online bool = true
//
//fmt.Println(name)   // выведет James
//fmt.Println(x)      // выведет 42
//fmt.Println(y)      // выведет 1.37
//fmt.Println(online) // выведет true

//var x bool
//fmt.Println(x)

//x := 42
//y := 8
//
//// сложение
//res := x + y
//fmt.Println(res)  // выведет 50
//
//// вычитание
//res = x - y
//fmt.Println(res)  // выведет 34
//
//// умножение
//res = x * y
//fmt.Println(res)  // выведет 336
//
//// деление
//res = x / y
//fmt.Println(res)  // выведет 5
//
//// остаток от деления
//res = x % y
//fmt.Println(res)  // выведет 2

//x := "привет "
//y := "мир"
//fmt.Println(x + y) // выведет "привет мир"

//x := 42
//y := 8
//
//x += y
//fmt.Println(x) // выведет 50
//
//x *= y
//fmt.Println(x) // выведет 400

//x := 42
//y := 8
//
//// равно
//fmt.Println(x == y) // выведет false
//
//// не равно
//fmt.Println(x != y) // выведет true
//
//// больше чем
//fmt.Println(x > y)  // выведет true
//
//// меньше чем
//fmt.Println(x < y)  // выведет false
//}

//func main() {
//	var gender int
//	switch gender {
//	case 1:
//		fmt.Println("Мужчина")
//	case 2:
//		fmt.Println("Женщина")
//	default:
//		fmt.Println("Еще не определился")
//	}
//}

//func main()  {
//	x := 7
//	res := 0
//	switch {
//	case x> 8:
//		res++
//	case x > 3 && x < 10:
//		res++
//	case x > 5:
//		res++
//	}
//	fmt.Println(res)
//}

//func main() {
//	height := 195
//	if height > 185 {
//		fmt.Println("Да")
//	} else {
//		fmt.Println("Нет")
//	}
//}

//package main
//
//import "fmt"
//
//func main() {
//	//Если сумма больше 1000 - вывести Apple
//	//Если сумма от 500 до 1000 (включительно) - вывести Samsung
//	//Если сумма меньше 500 - вывести Nokia с фонариком
//	var a int
//	fmt.Scan(&a)
//	switch {
//	case a > 1000:
//		fmt.Print("Apple")
//	case 1000 >= a && a >= 500:
//		fmt.Print("Samsung")
//	case a < 500:
//		fmt.Print("Nokia с фонариком")
//	}
//}

//res := 0
//for i := 0; i < 10; i += 3 {
//res += i
//}
//fmt.Println(res)

//package main
//
//import "fmt"
//
//func main() {
//	var a,b int
//	for a = 1; a < 4; a++ {
//		fmt.Scan(&b)
//		switch b {
//		case 0:
//			fmt.Println("Ноль")
//		case 1:
//			fmt.Println("Один")
//		case 2:
//			fmt.Println("Два")
//		case 3:
//			fmt.Println("Три")
//		case 4:
//			fmt.Println("Четыре")
//		case 5:
//			fmt.Println("Пять")
//		case 6:
//			fmt.Println("Шесть")
//		case 7:
//			fmt.Println("Семь")
//		case 8:
//			fmt.Println("Восемь")
//		case 9:
//			fmt.Println("Девять")
//		case 10:
//			fmt.Println("Десять")
//		}
//	}
//}

//package main
//
//import "fmt"
//
//func line() {
//	fmt.Println("-----")
//}
//
//func main() {
//	line()
//}

//package main
//
//import (
//	"fmt"
//)

//func max(a, b int) int {
//	if a >= b {
//		return a
//	} else {
//		return b
//	}
//}
//
//func calc(a int) (int, int) {
//	return a * 2, a * a
//}

package main

import (
	"fmt"
	"math"
	"strconv"
)

func isEven(a int) bool {
	return a%2 == 0
}

func double(a int) {
	fmt.Println(a * 2)
}

func double_m(a, b int) int {
	c := int(math.Min(float64(a), float64(b)))
	d := int(math.Max(float64(a), float64(b)))
	var summ int
	for i := c; i <= d; i++ {
		summ += i * i
	}
	return summ
}

func one_or_two(a, b int, sterak string) (int, string) {
	switch sterak {
	case "one":
		return a, sterak
	case "two":
		return b, sterak
	}
	return 0, sterak
}

func mars_age(earth_age int) int {
	return earth_age * 365 / 687
}

type Contact struct {
	name string
	age  int
}

func (x Contact) welcome() {
	fmt.Println(x.name)
	fmt.Println(x.age)
}

func (ptr *Contact) increase(val int) {
	ptr.age += val
}

func welcomez(x Contact) {
	fmt.Println(x.name)
	fmt.Println(x.age)
}

type T struct {
	val int
}

func (p *T) a() {
	p.val += 1
}
func (p T) b() {
	p.val += 2
}

func sum(nums ...int) {
	total := 0
	for _, v := range nums {
		total += v
	}
	fmt.Println(total)
}

func main() {
	fmt.Println(isEven(10))
	//for i := 4; i > 0; i-- {
	//	defer double(i)
	//}
	fmt.Println(double_m(2, 6))
	fmt.Println(one_or_two(3, 19, "zero"))
	fmt.Println("mars_age = " + strconv.Itoa(mars_age(33)))
	x := Contact{"Джеймс", 42}
	x.welcome()
	welcomez(x)
	x.increase(8)
	fmt.Println(x.age) // выведет 50

	fmt.Println()
	y := T{5}
	y.a()
	y.b()
	fmt.Println(y.val)

	//var a [5]int
	a := [5]int{0, 2, 4, 6, 8}
	fmt.Println(a)
	var s = a[1:3]
	zes := &s
	fmt.Println(&s)    // выведет [2 4]
	fmt.Println(zes)   // выведет [2 4]
	fmt.Println(a[1:]) // выведет [2 4]
	fmt.Println(a[:2]) // выведет [2 4]

	ze := make([]int, 5)
	ze = append(ze, 8)
	fmt.Println(ze) // выведет [0 0 0 0 0 8]

	xz := "hello"
	for _, c := range xz {
		fmt.Println(c) // Неожиданно, правда? Эта программа выводит код символов Unicode.
	}
	for _, c := range xz {
		fmt.Printf("%c \n", c)
	}
	m := make(map[string]int)
	m["James"] = 42
	m["Amy"] = 24
	m["Bob"] = 8

	fmt.Println(m["James"]) // выведет 42
	fmt.Println(m["Asya"])  // выведет 42
	delete(m, "James")

	mt := map[string]int{
		"James": 42,
		"Amy":   24}

	fmt.Println(mt["Amy"]) // выведет 24
	countries := map[string]string{
		"us": "USA",
		"fr": "France",
		"ch": "China",
	}
	fmt.Println(countries)
	fmt.Println("a", "b", "c") // выведет a b c
	sum(2, 4, 6)               // выведет 12
	sum(42, 8)                 // выведет 50
	sum(1, 2, 3, 4, 5, 6)      // выведет 21
	//results := []string{"w", "l", "w", "d", "w", "l", "l", "l", "d", "d", "w", "l", "w", "d"}
	//var input string
	//fmt.Scanln(&input)
	//results = append(results, input)
	//fmt.Println(results)
	//scores := 0
	//for _, res := range results {
	//	switch res {
	//	case "w":
	//		scores += 3
	//	case "d":
	//		scores += 1
	//	case "l":
	//		scores += 0
	//	}
	//}

}
