//package main
//
//import "fmt"
//
//func main() {
//	workArrayRaw := make([]uint8, 0, 0)
//	swapArray := make([]uint8, 0, 0)
//	workArray := make([]uint8, 10, 10)
//	var c, d uint8
//	for i := 0; i < 10; i++ {
//		fmt.Scan(&d)
//		workArrayRaw = append(workArrayRaw, d)
//	}
//	for i := 0; i < 6; i++ {
//		fmt.Scan(&c)
//		swapArray = append(swapArray, c)
//	}
//	//workArray = workArrayRaw
//	for i := 0; i < len(workArray); i++ {
//		workArray[i] = workArrayRaw[i]
//	}
//	for i := 0; i < 6; i += 2 {
//		workArray[swapArray[i]] = workArrayRaw[swapArray[i+1]]
//		workArray[swapArray[i+1]] = workArrayRaw[swapArray[i]]
//	}
//	for i := 0; i < len(workArray); i++ {
//		fmt.Print(workArray[i], " ")
//	}
//}

//package main
//
//import "fmt"
//
//func main() {
//	var c, d int
//	fmt.Scanln(&d)
//	Arr := make([]int, d, d)
//	for i := 0; i < d; i++ {
//		fmt.Scan(&c)
//		Arr[i] = c
//	}
//	fmt.Println(Arr[3])
//}

//package main
//import "fmt"
//
//func main()  {
//	array := [5]int{}
//	var a int
//	for i:=0; i < 5; i++{
//		fmt.Scan(&a)
//		array[i] = a
//	}
//	a = array[0]
//	for i:=0; i < 5; i++{
//		if array[i] > a {
//			a = array[i]
//		}
//	}
//	fmt.Println(a)
//}

//package main
//
//import "fmt"
//
//func main() {
//	var a, b int
//	fmt.Scan(&b)
//	array := make([]int, b, b)
//	for i := 0; i < b; i++ {
//		fmt.Scan(&a)
//		array[i] = a
//	}
//	for i := 0; i < b; i += 2 {
//		if i == 0 {
//			fmt.Print(array[i])
//		} else {
//			fmt.Print(" ", array[i])
//		}
//	}
//}

package main

import "fmt"

func main() {
	var a, b int
	fmt.Scan(&b)
	array := make([]int, b, b)
	for i := 0; i < b; i++ {
		fmt.Scan(&a)
		array[i] = a
	}
	a = 0
	for i := 0; i < b; i++ {
		if array[i] > 0 {
			a++
		}
	}
	fmt.Print(a)
}
