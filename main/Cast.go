//package main

//
//func adding(arg, art string) int64{
//	argrs := []rune(arg)
//	artrs := []rune(art)
//	buf := []rune("")
//
//	var argInt, artInt int64
//	for _, letter := range argrs {
//		if unicode.IsDigit(letter) {
//			buf = append(buf, letter)
//		}
//	}
//	argInt, _ = strconv.ParseInt(string(buf), 10, 0)
//
//	buf = []rune("")
//	for _, letter := range artrs {
//		if unicode.IsDigit(letter) {
//			buf = append(buf, letter)
//		}
//	}
//	artInt, _ = strconv.ParseInt(string(buf), 10, 0)
//	return argInt + artInt
//}

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var csv string
	csv, _ = bufio.NewReader(os.Stdin).ReadString('\n')
	csv = strings.Trim(csv, "\n")

	csv = strings.Replace(csv, " ", "", -1)
	csv = strings.Replace(csv, ",", ".", -1)
	arrOfTwo := strings.Split(csv, ";")

	var a, b float64
	a, _ = strconv.ParseFloat(arrOfTwo[0], 10)
	b, _ = strconv.ParseFloat(arrOfTwo[1], 10)
	fmt.Printf("%.4f", a/b)
}
