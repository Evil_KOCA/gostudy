//package main
//
//import "fmt"
//func externalFunction() func() {
//	text := "TEXT"
//
//	return func() {
//		fmt.Println(text)
//	}
//}
//
//func ExampleEnvironment() {
//	fn := externalFunction()
//	fn()
//
//	// Output:
//	// TEXT
//}
//func ExampleClosure() {
//	fn := func() func(int) int {
//		count := 0
//		return func(i int) int {
//			count++
//			return count * i
//		}
//	}()
//	fmt.Printf("%T\n",fn)
//	for i := 1; i <= 5; i++ {
//		fmt.Println(fn(i))
//	}
//
//	// Output:
//	// 1
//	// 4
//	// 9
//	// 16
//	// 25
//}
//func main()  {
//	x := func(fn func(i int) int, i int) func(int) int { return fn }(func(i int) int { return i + 1 }, 5)
//	fmt.Printf("%T\n", x)
//	ExampleEnvironment()
//	ExampleClosure()
//}

package main

import (
	"fmt"
	"strconv"
)

func main() {
	fn := func(a uint) uint {
		dict := map[byte]string{50: "2", 52: "4", 54: "6", 56: "8"}
		var result string
		var pre uint64
		bs := strconv.FormatUint(uint64(a), 10)
		for i := 0; i < len(bs); i++ {
			if _, inMap := dict[bs[i]]; inMap {
				result = result + dict[bs[i]]
			}
		}
		pre, _ = strconv.ParseUint(result, 0, 10)
		if pre == 0 {
			return uint(100)
		} else {
			return uint(pre)
		}
	}
	fmt.Println(fn(727178))
	fmt.Println(fn(0))
	fmt.Println(fn(17351753))
}
